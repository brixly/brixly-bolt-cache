# Enable YAML in PHP config for cPanel
#echo "extension=yaml.so" >> /usr/local/cpanel/3rdparty/php/72/etc/php.ini.dist
#/usr/local/cpanel/bin/checkphpini

# Enable YAML in cPanel if it doesn't already exist
grep -q "extension=yaml.so" /usr/local/cpanel/3rdparty/php/73/etc/php.ini.dist
if [[ $? != 0 ]]; then
    echo "extension=yaml.so" >> /usr/local/cpanel/3rdparty/php/73/etc/php.ini.dist && /usr/local/cpanel/bin/checkphpini;
fi;