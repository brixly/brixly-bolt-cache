    jQuery(document).ready(function() {

     jQuery('#smartwizard').smartWizard({
      useURLhash: false,
      selected: 0,
      keyNavigation: false,
      showStepURLhash: false
     });
    });

    var boltcache = {
     existingConfig_YAML: '',
     existingConfig_ARRAY: '',
     template_list: '',
     updatedConfig: '',
     url: '',
     selectDomain: function(selectOBJ) {
      var url = $(selectOBJ).data('url');
      $('#step-1').hide();
      $('#smartwizard').removeClass('hidden');
      boltcache.url = url;
      $('.selected_domain').html(url);
      boltcache.nextPage();
      boltcache.changeTab('WordPress');
      boltcache.getCurrentConfig(url);
      console.log(boltcache.existingConfig_ARRAY);
     },
     changeTab: function(tabID) {
      var tabID = 'tab_' + tabID;
      $('.tabcontent').hide();
      $('#' + tabID).show();
     },
     nextPage: function() {
      $('.sw-btn-next').click();
     },
     getCurrentConfig: function(url) {
      $.ajax({
       method: 'POST',
       data: {
        ajax: 'selectDomain',
        url: url,
        dataType: 'json',
        async: false
       },
       success: function(data) {
        var config_array = JSON.parse(data);
        boltcache.existingConfig_ARRAY = config_array;
        boltcache.displayExistingConfig(config_array);
       }
      });
     },
     displayExistingConfig: function(config) {
      console.log(config);
      $.ajax({
       method: 'POST',
       data: {
        ajax: 'getTemplates',
        dataType: 'json',
        async: false
       },
       success: function(data) {
        boltcache.template_list = JSON.parse(data);
        var current_tpl = boltcache.template_list[config.backend_category][config.apptemplate_code];
        $('#current_template').html(current_tpl);
        if (config.apptemplate_code == '1000.j2') {
         $('.reset-container').hide();
        } else {
         $('.reset-container').show();
        }
       }
      });
     },
     selectVersionPHP: function(obj) {

      var selected_version = $('#php_select').val();

      //alert($('#php_select').val());
      boltcache.updatedConfig.backend_version = $('#php_select').val();

      // Set backend path
      if (selected_version == 'CPANELPHP80') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php80/root';
      if (selected_version == 'CPANELPHP74') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php74/root';
      if (selected_version == 'CPANELPHP73') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php73/root';
      if (selected_version == 'CPANELPHP72') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php72/root';
      if (selected_version == 'CPANELPHP71') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php71/root';
      if (selected_version == 'CPANELPHP70') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php70/root';
      if (selected_version == 'CPANELPHP56') boltcache.updatedConfig.backend_path = '/opt/cpanel/ea-php56/root';

      //console.log(boltcache.updatedConfig);
      boltcache.saveConfig();
      boltcache.nextPage();

     },
     isEmpty: function(obj) {
      for (var key in obj) {
       if (obj.hasOwnProperty(key))
        return false;
      }
     },
     saveConfig: function() {

      // Correct 'subdir_apps'
      if (boltcache.updatedConfig.subdir_apps === undefined || boltcache.updatedConfig.subdir_apps.length == 0 || boltcache.isEmpty(boltcache.updatedConfig.subdir_apps)) {
       boltcache.updatedConfig.subdir_apps = {};
      }

      console.log(boltcache.updatedConfig);

      var updatedConfig = JSON.stringify(boltcache.updatedConfig);

      $.ajax({
       method: 'POST',
       //dataType: 'json',
       data: {
        ajax: 'saveChanges',
        url: boltcache.url,
        json: updatedConfig
       },
       success: function(data) {
        //boltcache.existingConfig_ARRAY = JSON.parse(data);
        console.log(data);
       }
      });

     },
     setTemplate: function(template) {

      var yaml_config = boltcache.existingConfig_ARRAY;

      // Set Defaults
      yaml_config.gzip = "enabled";
      yaml_config.set_expire_static = "enabled";

      switch (template) {

       // Generic Templates

       case "default_proxy":
        yaml_config.apptemplate_code = "1000.j2";
        yaml_config.backend_category = "PROXY";
        yaml_config.backend_version = "httpd";
        yaml_config.backend_path = 0
        boltcache.updatedConfig = yaml_config;
        boltcache.saveConfig();
        boltcache.nextPage();
        break;

       case "proxy_cache":
        yaml_config.apptemplate_code = "1005.j2";
        yaml_config.backend_category = "PROXY";
        yaml_config.backend_version = "httpd";
        yaml_config.backend_path = 0
        break;

        // WordPress Templates

       case "wp_nginx":
        yaml_config.apptemplate_code = "5001.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

       case "wp_redis":
        yaml_config.apptemplate_code = "wpredis_auto.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        yaml_config.mod_security = "disabled";
        yaml_config.pagespeed = "disabled";
        break;

       case "wp_rocket":
        yaml_config.apptemplate_code = "5048.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

        // Joomla Templates

       case "joomla_nginx":
        yaml_config.apptemplate_code = "5002.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

        // Magento Templates

       case "magento_nginx":
        yaml_config.apptemplate_code = "5003.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

       case "magento_nginx_v2":
        yaml_config.apptemplate_code = "5025.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

        // Drupal Templates

       case "drupal_nginx":
        yaml_config.apptemplate_code = "5016.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

       case "drupal_redis":
        yaml_config.apptemplate_code = "drupalredis_auto.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        yaml_config.mod_security = "disabled";
        yaml_config.pagespeed = "disabled";
        break;

        // WHMCS Templates

       case "whmcs_nginx":
        yaml_config.apptemplate_code = "5021.j2";
        yaml_config.backend_category = "PHP";
        yaml_config.backend_version = "CPANELPHP72";
        break;

      }

      boltcache.nextPage();

      boltcache.updatedConfig = yaml_config;

     }
    }
