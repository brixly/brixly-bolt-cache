<?php
//phpinfo();
error_reporting(E_ALL);
//phpinfo();
ini_set('display_errors', 1);
include ("/usr/local/cpanel/php/cpanel.php"); // Instantiate the CPANEL object.
$cpanel = new CPANEL(); // Connect to cPanel - only do this once.
if (!function_exists('yaml_parse'))
{
    echo "YAML Functionality is required for this module to work. Please contact your system administrator.";
    die();
}

if (isset($_GET['ajax']) || isset($_POST['ajax']))
{

    if ($_POST['ajax'] == 'selectDomain')
    {

        // Get the current configuration and parse YAML from /opt/nDeploy/domain-data/domain
        $domain_config = file_get_contents('/opt/nDeploy/domain-data/' . $_POST['url']);
        $yaml_array = yaml_parse($domain_config);
        echo json_encode($yaml_array);
        die();

    }

    if ($_POST['ajax'] == 'getTemplates')
    {

        // Get the list of templates from /opt/nDeploy/conf/apptemplates.yaml
        $template_list = file_get_contents('/opt/nDeploy/conf/apptemplates.yaml');
        $yaml_array = yaml_parse($template_list);
        echo json_encode($yaml_array);
        die();

    }

    if ($_POST['ajax'] == 'saveChanges')
    {

        // Convert JSON to YAML
        echo "POST OUTPUT: <br />";
        print_r($_POST);

        $new_config = json_decode($_POST['json'], true);

        echo "JSON DECODE: <br />";
        print_r($new_config);

        $yaml_config = yaml_emit($new_config);

        // Replace [] with {} for correct serialisation
        $yaml_config = str_replace("[]", "{}", $yaml_config);

        echo "YAML OUTPUT: <br />";
        print_r($yaml_config);

        $filename = '/opt/nDeploy/domain-data/' . $_POST['url'];
        echo file_put_contents($filename, $yaml_config);
        die();

    }
}

print $cpanel->header("Bolt-Cache"); // Add the header.
// List the account's domains.
$get_domain_list = $cpanel->uapi('DomainInfo', 'list_domains');
$get_domain_list = $get_domain_list['cpanelresult']['result']['data'];

?>
<img src='bolt-cache/Black_logo_-_no_background.svg' class='img img-responsive' style='max-width: 350px;'>
<hr />
  <link href="bolt-cache/smart_wizard.css" rel="stylesheet" />
  <div class="top-notice">
	<h4>What is Bolt-Cache?</h4>
	
	<p>Bolt-Cache is a plugin for cPanel, built by our developers in-house to drastically improve site loading speeds for WordPress (and many other) applications / sites.</p>

	<p>We use a unique 'stack' for our web hosting platform - by default its incredibly fast, but can be further 'tuned' to enhance performance. Bolt-Cache makes that process incredibly simple, with a 3 click interface giving you access to some of the worlds fastest caching technologies.</p>
	
	<p>For more advanced control, and for more caching options please refer to our CloudNS plugin available from cPanel -> CloudNS.</p>

  </div>

  <div id="step-1" class="" style="background-color: white;">
			<legend>Select a Domain</legend>
			<table class="table table-hover table-dark">
			  <thead>
			    <tr>
			      <th scope="col">Domain</th>
			      <th scope="col"></th>
			    </tr>
			  </thead>
			  <tbody>

			    <tr>
			      	<th scope="row"><?php echo $get_domain_list['main_domain']; ?>  <span class="badge badge-secondary hide" style="font-size: 10px;">Proxy / Default</span></th>
			      	<td>
                <input class="btn btn-info pull-right btn" type="button" value="Setup Caching" data-url="<?php echo $get_domain_list['main_domain']; ?>" onclick="boltcache.selectDomain(this);"/>
                <button class="btn btn-info pull-right btn hidden" type="button" value="Pagespeed Optimisations" style="margin-right: 10px;" data-url="<?php echo $get_domain_list['main_domain']; ?>" onclick="boltcache.selectDomain(this);"><span class="fa fa-google"></span>Pagespeed Tweaks</button>
                  
                </button>
              </td>
			    </tr>

			    <?php 
          if(!empty($get_domain_list['addon_domains'])) {
          foreach ($get_domain_list['addon_domains'] as $domain) {
		  
		  	$server_name = $cpanel->uapi('DomainInfo', 'single_domain_data', array(
                'domain' => $domain
            	));
			$domain_name = $server_name['cpanelresult']['result']['data']['domain'];
			$server_name = $server_name['cpanelresult']['result']['data']['servername'];
		  
             ?>
			    
              <tr>
                  <th scope="row"><?php echo $domain_name; ?></th>
                  <td><input class="btn btn-info pull-right" type="button" value="Setup Caching" data-url="<?php echo $server_name; ?>" onclick="boltcache.selectDomain(this);"/></td>
              </tr>

            <?php
          }}; ?>


			    <?php foreach ($get_domain_list['sub_domains'] as $domain)
{ ?>
			    
				    <tr>
				      	<th scope="row"><?php echo $domain; ?></th>
				      	<td><input class="btn btn-info pull-right" type="button" value="Setup Caching" data-url="<?php echo $domain; ?>" onclick="boltcache.selectDomain(this);"/></td>
				    </tr>

			    <?php
}; ?>

			  </tbody>
			</table>

  </div>

  <div class="centered">

    <div id="smartwizard" class="hidden">
      <ul>
        <li style="display: none"><a href="#step-1">Select your domain<br /><small>Step One</small></a></li>
        <li><a href="#step-2">Select the application type<br /><small>Step One</small></a></li>
        <li><a href="#step-3">Select the PHP Version<br /><small>Step Two</small></a></li>
        <li><a href="#step-4">Done!<br /><small>Step Three</small></a></li>
      </ul>

      <div>
        <div id="step-2" class="">
        	<div class="row" style="margin: 0;">
	            <legend>Select an Application<span class="selected_domain pull-right"></span></legend>
				<div id="existing-config">
					We have detected you are currently using the '<span id="current_template"></span>' Template.
				</div>
	            <div class="alert alert-warning reset-container">
	            	<div class="row">
	            	<div class="col-sm-10" style="font-size: 13px;">
	            	Important: By default your sites will be served by Apache + Litespeed, however to increase site loading times the performance optimisations below will <b>remove</b> Apache from your stack. This means only nginx + PHP-FPM will be used to serve your sites content. Whilst this is faster, this does mean that <b>custom .htaccess rules, and the 'Select PHP Version' screen will no longer work</b>. You can reset the configuration to the default 'proxy' mode here.
	            	</div>
	            	<div class="col-sm-2">
				  		<button type="button" class="btn btn-warning pull-right btn-block" onclick="boltcache.setTemplate('default_proxy');">RESET / DISABLE</button>
				  	</div>
				  </div>
	            </div>

	            <div class="clear">
					<div class="tab">
						<button class="tablinks" onclick="boltcache.changeTab('WordPress'); return false;" id="defaultOpen"><img src="bolt-cache/wp.png" class="img img-responsive"></button>
						<button class="tablinks" onclick="boltcache.changeTab('WHMCS'); return false;"><img src="bolt-cache/whmcs.png" class="img img-responsive"></button>
						<button class="tablinks" onclick="boltcache.changeTab('Joomla'); return false;"><img src="bolt-cache/joomla.png" class="img img-responsive"></button>
						<button class="tablinks" onclick="boltcache.changeTab('Magento'); return false;"><img src="bolt-cache/magento.png" class="img img-responsive"></button>
						<button class="tablinks" onclick="boltcache.changeTab('Drupal'); return false;"><img src="bolt-cache/drupal.png" class="img img-responsive"></button>
				</div>

					<div id="tab_WordPress" class="tabcontent">
						<h3>WordPress</h3>
						<p>By default, your WordPress site will be using our ‘nginx first’ reverse proxy to httpd / apache (running mod_lsapi) – this is incredibly performant by default, however you are able to enable everything from full page caching through to nginx-only support for some of the most popular WordPress caching plugins.</p>

						<div class="card-deck text-center">
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/litespeed.png" style="max-width: 30%;" />Proxy Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-success">.htaccess compatible</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Proxy cache still allows full use of the .htaccess file and is therefore still ‘reverse proxied’ to Litespeed PHP. Enabling Proxy Cache enables caching your static resources, such as images, CSS, JS into the server memory, and works in a similar way to the CloudFlare CDN.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('proxy_cache');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/nginx.png" style="max-width: 20%; margin: 5%;" />NGINX</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Enabling this option will remove the use of Apache from your website, replacing it with native NGINX and PHP-FPM. This is known to be one of the fastest ways to serve PHP pages without the need for heavy server-side caching. Ideal for dynamic websites and WooCommerce.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('wp_nginx');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/rocket.png" style="max-width: 20%; margin: 5%;" />WP Rocket</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">This option will enable the same features as provided by ‘Rocket Nginx’ which is an nginx set of directives designed to increase loading times for your WordPress website when using the WP Rocket plugin. WP Rocket requires a purchased licence (3rd Party Product).</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('wp_rocket');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/redis.png" style="max-width: 20%; margin: 5%;" />Full-Page Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Redis (or 'Full Page Cache') allows you to serve your website or blog to your visitors as though it were ‘static’. This is the fastest way to serve sites that are less frequently updated, as the entire contents are stored in memory. Caches are cleared every 10 minutes automatically.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('wp_redis');">Enable</button>
						          </div>
						        </div>
						</div>

					</div>


					<div id="tab_WHMCS" class="tabcontent">
						<h3>WHMCS</h3>
						<p>By default, your WHMCS site will be using our ‘nginx first’ reverse proxy to httpd / apache (running mod_lsapi) – this is incredibly performant by default, however you are able to enable proxy caching to speed up the requests to your site, or disable Apache and use PHP-FPM / NGINX.</p>

						<div class="card-deck text-center">
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/litespeed.png" style="max-width: 30%;" />Proxy Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-success">.htaccess compatible</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Proxy cache still allows full use of the .htaccess file and is therefore still ‘reverse proxied’ to Litespeed PHP. Enabling Proxy Cache enables caching your static resources, such as images, CSS, JS into the server memory, and works in a similar way to the CloudFlare CDN.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('proxy_cache');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/nginx.png" style="max-width: 20%; margin: 5%;" />NGINX</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Enabling this option will remove the use of Apache from your website, replacing it with native NGINX and PHP-FPM. This is known to be one of the fastest ways to serve PHP pages without the need for heavy server-side caching. Ideal for dynamic websites and Joomla.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('whmcs_nginx');">Enable</button>
						          </div>
						        </div>
						</div>
					</div>


					<div id="tab_Joomla" class="tabcontent">
						<h3>Joomla</h3>
						<p>By default, your Jooma site will be using our ‘nginx first’ reverse proxy to httpd / apache (running mod_lsapi) – this is incredibly performant by default, however you are able to enable proxy caching to speed up the requests to your site, or disable Apache and use PHP-FPM / NGINX.</p>

						<div class="card-deck text-center">
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/litespeed.png" style="max-width: 30%;" />Proxy Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-success">.htaccess compatible</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Proxy cache still allows full use of the .htaccess file and is therefore still ‘reverse proxied’ to Litespeed PHP. Enabling Proxy Cache enables caching your static resources, such as images, CSS, JS into the server memory, and works in a similar way to the CloudFlare CDN.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('proxy_cache');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/nginx.png" style="max-width: 20%; margin: 5%;" />NGINX</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Enabling this option will remove the use of Apache from your website, replacing it with native NGINX and PHP-FPM. This is known to be one of the fastest ways to serve PHP pages without the need for heavy server-side caching. Ideal for dynamic websites and Joomla.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('joomla_nginx');">Enable</button>
						          </div>
						        </div>
						</div>
					</div>

					<div id="tab_Magento" class="tabcontent">
						<h3>Magento</h3>
						<p>By default, your Magento site will be using our ‘nginx first’ reverse proxy to httpd / apache (running mod_lsapi) – this is incredibly performant by default, however you are able to enable everything from full page caching through to proxy cache.</p>

						<div class="card-deck text-center">
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/litespeed.png" style="max-width: 30%;" />Proxy Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-success">.htaccess compatible</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Proxy cache still allows full use of the .htaccess file and is therefore still ‘reverse proxied’ to Litespeed PHP. Enabling Proxy Cache enables caching your static resources, such as images, CSS, JS into the server memory, and works in a similar way to the CloudFlare CDN.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('proxy_cache');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/nginx.png" style="max-width: 20%; margin: 5%;" />NGINX</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Enabling this option will remove the use of Apache from your website, replacing it with native NGINX and PHP-FPM. This is known to be one of the fastest ways to serve PHP pages without the need for heavy server-side caching. Compatible with Magento v1 and v2.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('magento_nginx');">Enable Magento v1</button>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('magento_nginx_v2');">Enable Magento v2</button>
						          </div>
						        </div>
						</div>

					</div>

					<div id="tab_Drupal" class="tabcontent">
						<h3>Drupal</h3>
						<p>By default, your Drupal site will be using our ‘nginx first’ reverse proxy to httpd / apache (running mod_lsapi) – this is incredibly performant by default, however you are able to enable everything from full page caching through to proxy cache.</p>

						<div class="card-deck text-center">
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/litespeed.png" style="max-width: 30%;" />Proxy Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-success">.htaccess compatible</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Proxy cache still allows full use of the .htaccess file and is therefore still ‘reverse proxied’ to Litespeed PHP. Enabling Proxy Cache enables caching your static resources, such as images, CSS, JS into the server memory, and works in a similar way to the CloudFlare CDN.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('proxy_cache');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/nginx.png" style="max-width: 20%; margin: 5%;" />NGINX</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Enabling this option will remove the use of Apache from your website, replacing it with native NGINX and PHP-FPM. This is known to be one of the fastest ways to serve PHP pages without the need for heavy server-side caching. Ideal for dynamic websites and WooCommerce.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('drupal_nginx');">Enable</button>
						          </div>
						        </div>
						        <div class="card col-sm-3 box-shadow">
						          <div class="card-header">
						            <h4 class="my-0 font-weight-normal"><img src="bolt-cache/redis.png" style="max-width: 20%; margin: 5%;" />Redis Cache</h4>
						          </div>
						          <div class="card-body">
						          	<div class="star-rating">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
									</div>
						          	<span class="badge badge-warning">.htaccess not supported</span>
						            <ul class="list-unstyled mt-3 mb-4">
						              <p style="font-size: 10px; padding: 10px;">Redis (or 'Full Page Cache') allows you to serve your website or blog to your visitors as though it were ‘static’. This is the fastest way to serve sites that are less frequently updated, as the entire contents are stored in memory. Caches are cleared every 10 minutes automatically.</p>
						            </ul>
						            <button type="button" class="btn btn-lg btn-block btn-primary" onclick="boltcache.setTemplate('drupal_redis');">Enable</button>
						          </div>
						        </div>
						</div>

					</div>

				</div>
			</div>
        </div>
        <div id="step-3" class="container">
	            <legend>Select your required PHP Version<span class="selected_domain pull-right"></span></legend>

				<div class="alert alert-warning hide">
	            	<div class="row">
	            	<div class="col-sm-10" style="font-size: 13px;">
	            	Important: By default your sites will be served by Apache + Litespeed, however to increase site loading times the performance optimisations below will <b>remove</b> Apache from your stack. This means only nginx + PHP-FPM will be used to serve your sites content. Whilst this is faster, this does mean that <b>custom .htaccess rules, and the 'Select PHP Version' screen will no longer work</b>. You can reset the configuration to the default 'proxy' mode here.
	            	</div>
	            	<div class="col-sm-2">
				  		<button type="button" class="btn btn-warning pull-right btn-block" onclick="boltcache.setTemplate('default_proxy');">RESET / DISABLE</button>
				  	</div>
				  </div>
	            </div>

	            <div class="row">
	            	<div class="col-sm-10">
					<select class="form-control form-control-lg" id="php_select">
					  <option value="CPANELPHP80">PHP 8.0</option>
					  <option value="CPANELPHP74">PHP 7.4 (Recommended)</option>
					  <option value="CPANELPHP73">PHP 7.3</option>
					  <option value="CPANELPHP72">PHP 7.2</option>
					  <option value="CPANELPHP71">PHP 7.1</option>
					  <option value="CPANELPHP70">PHP 7.0</option>
					  <option value="CPANELPHP56">PHP 5.6</option>
					</select>
					</div>
					<div class="col-sm-2">
						<input class="btn btn-info pull-right btn-lg btn-block" type="button" value="Save Changes" data-url="anotherdemo.com" onclick="boltcache.selectVersionPHP(this);">
					</div>
				</div>

        </div>
        <div id="step-4" class="">
          <div class="inner row">
			<div class="col-sm-8">
				<div class="alert alert-success">Done! Your changes have now been saved.</div>
						<legend>Whats next?</legend>
						<p><strong>Nothing!</strong> Thats it, the performance enhancements have been made to your site!</p>
            <p>Please note that the changes may take up to 5 minutes to take effect. </p>
			</div>
			<div class="col-sm-4">
				<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/mi6DsSSNKDbUY" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed"></iframe></div><p><a href="https://giphy.com/gifs/space-rocket-mi6DsSSNKDbUY"></a></p>
			</div>
           </div>

        </div>


        </div>
      </div>
    </div>





  </div>


  <?php
//var_dump("CONTAINS_MOD: $contains_mod");
// AutoSSL - Force install of domain
// Open URL in new window
print $cpanel->footer(); // Add the footer.
$cpanel->end(); // Disconnect from cPanel - only do this once.

?>


<style>

#content {
	margin-top: 0;
	padding-top: 35px;
	padding-bottom: 35px;
	min-height: 300px;
}

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 21.2%;
}

.top-notice {
	background: #12181d;
    padding: 15px;
    border-radius: 5px;
    font-size: 12px;
    color: white;
	margin-bottom: 35px;
	border: 2px solid #ff7101;
}

/* Style the buttons that are used to open the tab content */
.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

#current_template {
	font-weight: bold;
}

/* Create an active/current "tab button" class */
.tab button.active {
    background-color: #ccc;
}

#existing-config {
    margin: 10px 0;
    padding: 10px;
    background-color: #def0ff;
    border-radius: 5px;
    border: 1px solid #12181d;
    color: #1e7756;
}

/* Style the tab content */
.tabcontent {
    display: none;
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 78.8%;
    border-left: none;
}

.sw-btn-group {
  float: right !important;
}

.sw-btn-prev {
  float: left !important;
}

.tablinks img {
  max-width: 60%;
  margin: 0 auto;
}

.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-deck .card {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex: 1 0 0%;
    flex: 1 0 0%;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    margin-right: 15px;
    margin-bottom: 0;
    margin-left: 15px;
    padding-left: 0 !important;
    padding-right: 0 !important;
}

.box-shadow {
    box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .05);
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.card-body {
	padding: 10px;
}

.card.col-sm-3 {
    width: 24%;
    margin: 4px !important;
}

.badge-success {
    color: #fff;
    background-color: #28a745;
}

.badge-warning {
    color: #212529;
    background-color: #ffc107;
}

.checked {
    color: orange;
}

.star-rating {
	padding: 10px;	
}

.cpanel_body {
    max-width: none !important;
}

#devWarningBlock {
	display: none !important;	
}

legend {
	padding: 20px !important;	
}

.modal {
    overflow: initial;

}

.selected_domain {
	font-size: 18px;
    font-weight: bold;
    color: #b9b9b9;
}


.form-control-lg {
    height: calc(3.875rem + 10px);
    padding: 0.5rem 2rem;
    font-size: 1.75rem;
    line-height: 1.5;
    border-radius: .3rem;
}

.btn-toolbar {display: none;}

.container {width: 100%}

.sw-theme-default > ul.step-anchor > li.active > a {
    border: none !important;
    color: #fff !important;
    background: transparent !important;
    cursor: pointer;
	font-weight: bold;
}

.sw-theme-default > ul.step-anchor > li > a::after {
    content: "";
    background: #ff7101;
    height: 4px;
    position: absolute;
    width: 100%;
    left: 0px;
    bottom: 0px;
    -webkit-transition: all 250ms ease 0s;
    transition: all 250ms ease 0s;
    -webkit-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
}

.sw-theme-default > ul.step-anchor > li.done > a {
    border: none !important;
    color: #a9a9a9 !important;
    background: transparent !important;
    cursor: pointer;
}

.sw-theme-default .step-content {
    padding: 10px;
    border: 0px solid #D4D4D4;
    background-color: #FFF;
    text-align: left;
    border-radius: 0 0 5px 5px;
}
  
 .page-title {
  color: white;
}
  .page-header {
    display: none;
  }

  .step-anchor {
	  background-color: #12181d;
  }

</style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script>
	$ = jQuery;
  </script>
<script src="https://kit.fontawesome.com/cbe3e15c51.js" crossorigin="anonymous"></script>
<script src="bolt-cache/bolt-cache.js"></script>
<script src="bolt-cache/jquery.smartWizard.js"></script>


      <script>
      $selectedDomain = $('#domainSelection').val();

      //Step One
      $('#domainSelection').on('change', function() {
        $selectedDomain = $('#domainSelection').val();
        $('.domainSelected').html($selectedDomain);
      });

      //Step Two
      $(".app").on('click', function() {
        var n = $(this).attr("value");
        $("input[value='" + n + "']").removeClass('active').removeAttr('checked');
        $(this).addClass('active').attr('checked', 'checked');
        $selectedApplication = $(this).val();
        $('.applicationSelected').html($selectedApplication);
        //Step Three
        //alert($selectedApplication);
        var $selectedApplicationOptions = $selectedApplication.toLowerCase();
        // alert($selectedApplicationOptions);
        $('#' + $selectedApplicationOptions + '_options').show();
      });
    </script>
