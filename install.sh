# Single-line installation - 
# curl -s https://gitlab.com/brixly/brixly-bolt-cache/raw/master/install.sh | bash

cd /root
rm -Rf /root/brixly-bolt-cache/

git clone https://gitlab.com/brixly/brixly-bolt-cache.git
/scripts/install_plugin /root/brixly-bolt-cache
/scripts/install_plugin /root/brixly-bolt-cache --theme=jupiter
yes | cp -R /root/brixly-bolt-cache/bolt-cache.live.php /root/brixly-bolt-cache/bolt-cache /usr/local/cpanel/base/frontend/paper_lantern/.

yes | cp -R /root/brixly-bolt-cache/bolt-cache.live.php /root/brixly-bolt-cache/bolt-cache /usr/local/cpanel/base/frontend/jupiter/.

# Enable YAML in cPanel PHP
echo "extension=yaml.so" >> /usr/local/cpanel/3rdparty/php/73/etc/php.ini.dist && /usr/local/cpanel/bin/checkphpini;
echo "extension=yaml.so" >> /usr/local/cpanel/3rdparty/php/81/etc/php.ini.dist && /usr/local/cpanel/bin/checkphpini;


# Create post upcp hook
yes | cp /root/brixly-bolt-cache/enable_yaml.sh /opt/enable_yaml.sh && chmod 755 /opt/enable_yaml.sh
/usr/local/cpanel/bin/manage_hooks add script /opt/enable_yaml.sh --category System --event upcp --stage post --manual

# Add Crontab Entry
echo "*/10 * * * * root /opt/enable_yaml.sh" > /etc/cron.d/enable_yaml

yum -y install libyaml-devel
autodetect | /usr/local/cpanel/3rdparty/bin/pecl install yaml

